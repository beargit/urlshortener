'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const router = express.Router();

function getModel() {
    return require('./model');
}

// Automatically parse request body as JSON
router.use(bodyParser.json());


router.get('/:code', (req, res, next) => {
    getModel().get(req.params.code, (err, entity) => {
        if (err) {
            next(err);
            return;
        }
        res.status(200).json(entity);
    });
});

router.post('/', (req, res, next) => {
    getModel().create(req.body, (err, entity) => {
        if (err) {
            next(err);
            return;
        }
        res.status(200).json(entity);
    });
});

// Errors on "/api/url/*" routes.
router.use((err, req, res, next) => {
    // Format error and forward to generic error handler for logging and
    // responding to the request
    err.response = {
        message: err.message,
        internalCode: err.code,
    };
    next(err);
});

module.exports = router;
