'use strict';

const shortid = require('shortid');
const validUrl = require('valid-url');
const Datastore = require('@google-cloud/datastore');

const ds = Datastore({
    projectId: 'urlshortering',
});
const kind = 'UrlShorten';

// function fromDatastore(obj) {
//     obj.id = obj[Datastore.KEY].id;
//     return obj;
// }

function update(id, data, cb) {
    console.log('model update()');
    
    let key;
    let urlCode = shortid.generate();

    if (id) {
        key = ds.key([kind, parseInt(id, 10)]);
    } else {
        key = ds.key(kind);
    }

    const entity = {
        key: key,
        data: {
            urlCode: urlCode,
            originalUrl: data.originalUrl
        }
    };

    ds.save(entity, err => {
        // console.log('saved entity', entity);

        data.id = entity.key.id;
        data.urlCode = entity.data.urlCode;
        cb(err, err ? null : data);
    });
}

function create(data, cb) {
    console.log('model create()');
    const originalUrl = data.originalUrl;

    if (validUrl.isUri(originalUrl)) {
        const q = ds
            .createQuery([kind])
            .filter('originalUrl', originalUrl)
            .limit(1);

        ds.runQuery(q, (err, entities) => {
            if (err) {
                cb(err);
                return;
            }

            if (entities.length) {
                console.log('already exist', entities);
                entities[0].id = entities[0][Datastore.KEY].id;

                cb(null, entities[0])
            } else {
                console.log('no entity');
                update(null, data, cb);
            }
        });
    } else {
        cb({
            code: 401,
            message: 'Invalid Url'
        });
    }
}

function get(data, cb) {
    console.log('model get()');
    const q = ds
        .createQuery([kind])
        .filter('urlCode', data);

    ds.runQuery(q, (err, entity) => {
        if (err) {
            cb(err);
            return;
        }

        cb(null, entity);
    });
}

function read(id, cb) {
    console.log('model read()');
    const key = ds.key([kind, parseInt(id, 10)]);
    ds.get(key, (err, entity) => {
        if (!err && !entity) {
            err = {
                code: 404,
                message: 'Not found',
            };
        }
        if (err) {
            cb(err);
            return;
        }
        cb(null, entity);
    });
}

module.exports = {
    create,
    get,
    read
};