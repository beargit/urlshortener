'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const router = express.Router();

function getModel() {
    return require('./model');
}

// Automatically parse request body as form data
router.use(bodyParser.urlencoded({
    extended: false
}));

// Set Content-Type for all responses for these routes
router.use((req, res, next) => {
    res.set('Content-Type', 'text/html');
    next();
});

router.get('/', (req, res, next) => {
    res.render('web/index.pug', {
        shorturl: {},
        action: 'Add'
    });
});

router.post('/', (req, res, next) => {
    const data = req.body;

    // Save the data to the database.
    getModel().create(data, (err, savedData) => {
        if (err) {
            console.log('err', err);
            
            next(err);
            return;
        }

        console.log('savedData', savedData);
        
        res.redirect(`${req.baseUrl}/${savedData.id}`);
    });
});

router.get('/:id', (req, res, next) => {    
    getModel().read(req.params.id, (err, entity) => {
        if (err) {
            next(err);
            return;
        }

        console.log('view url', entity);
        res.render('web/view.pug', {
            shorturl: entity
        });
    });
});

router.get('/url/:code', (req, res, next) => {
    getModel().get(req.params.code, (err, entity) => {
        if (err) {
            next(err);
            return;
        }

        console.log('entity', entity);
        res.redirect(entity[0].originalUrl);
    });
});


// Errors on "/web/*" routes
router.use((err, req, res, next) => {
    // Format error and forward to generic error handler for logging and
    // responding to the request
    err.response = err.message;
    next(err);
});

module.exports = router;
