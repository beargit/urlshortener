'use strict';

const express = require('express');
const path = require('path');

const app = express();


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use('/web', require('./shorturls/crud'));
app.use('/api/url', require('./shorturls/api'));


// Redirect root to web
app.get('/', (req, res) => {
    res.redirect('/web');
});

app.get('/:code', (req, res) => {
    res.redirect('/web/url/'+ req.params.code);
});

// 404 error handler
app.use((req, res) => {
    res.status(404).send('Not Found');
});

// 500 error handler
app.use((err, req, res) => {
    console.error(err);
    res.status(500).send(err.response || 'Something broke!');
});

if (module === require.main) {
    // Start the server
    const server = app.listen(8080, () => {
        const port = server.address().port;
        console.log(`App listening on port ${port}`);
    });
}

module.exports = app;
